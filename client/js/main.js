import ViewUpdater from './ViewUpdater';
import WebSocketsClient from './WebSocketsClient';
import configureTooltip from './tooltip';

function main() {
    const webSocketsClient = new WebSocketsClient(`ws://${window.location.host}`);
    webSocketsClient.handleConnection();

    const viewUpdater = new ViewUpdater();

    setWebSocketsListeners(webSocketsClient, viewUpdater);
    setViewUpdaterListeners(webSocketsClient, viewUpdater);

    configureTooltip();
}

function setWebSocketsListeners(webSocketsClient, viewUpdater) {
    webSocketsClient.on('connect', () => {
        ViewUpdater.updateSplashState('connected');
    });

    webSocketsClient.on('error', () => {
        ViewUpdater.updateOnlineStatus(false);
        webSocketsClient.retryConnect(5000);
    });

    webSocketsClient.on('close', () => {
        ViewUpdater.updateOnlineStatus(false);
        webSocketsClient.retryConnect(5000);
    });

    webSocketsClient.on('online', (message) => {
        if (message.data) {
            if (message.data.connected) {
                ViewUpdater.updateSplashState('online');
                viewUpdater.prepareBody();
                ViewUpdater.updateOnlineStatus(true);
                viewUpdater.bindListeners();

                webSocketsClient.bindListeners();
                webSocketsClient.requestServerData();
            }
            ViewUpdater.updateOnlineStatus(message.data.connected);
        }
    });

    webSocketsClient.on('current-station', (message) => {
        ViewUpdater.updateCurrentStation(message.data.station_name);
    });

    webSocketsClient.on('stations-list', (message) => {
        viewUpdater.updateRadioList(message.data.stations, webSocketsClient);
    });

    webSocketsClient.on('status', (message) => {
        ViewUpdater.updatePlayingStatus(message.data.state);
        ViewUpdater.updateVolume(message.data.volume);
    });

    webSocketsClient.on('current-song', (message) => {
        ViewUpdater.updateCurrentSong(message.data.title || message.data.name);
    });
}

function setViewUpdaterListeners(webSocketsClient, viewUpdater) {
    viewUpdater.on('radio-change', (radioName) => webSocketsClient.sendRadioChange(radioName));

    viewUpdater.on('radio-add', (radioName, radioUrl) => webSocketsClient.sendCreateStation(radioName, radioUrl));

    viewUpdater.on('radio-edit', (radioName, newRadioName, radioUrl) => webSocketsClient.sendUpdateStation(radioName, newRadioName, radioUrl));

    viewUpdater.on('radio-delete', (radioName) => webSocketsClient.sendDeleteStation(radioName));

    viewUpdater.on('pause', () => webSocketsClient.sendPause());

    viewUpdater.on('play', () => webSocketsClient.sendPlay());

    viewUpdater.on('volume', (newVolume) => webSocketsClient.sendVolume(newVolume));
}

main();