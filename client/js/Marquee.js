import $ from 'jquery';

export default class Marquee {
    constructor($element, speed = 8, interval = 8) {
        this.$element = $element;
        this.speed = speed;
        this.interval = interval;

        this.timeout = null;
    }

    marquee() {
        const elementText = this.$element.text();
        this.$textSpan = $('<span>', {
            text: elementText,
            css: {
                display: 'inline-block'
            }
        });

        this.$element.html(this.$textSpan);
        this.startMarqueeAfterTimeout(2);

        $(window).on('resize.marquee', () => this.startMarqueeAfterTimeout(2));
    }

    startMarquee() {
        this.$textSpan.css('paddingRight', '5em');

        const $textSpanClone = this.$textSpan.clone();
        $textSpanClone.insertAfter(this.$textSpan);

        $({t: 0}).animate({t: 1}, {
            // easing: 'linear',
            duration: this.speed * 1000,
            step: (now) => {
                this.$textSpan.css('transform', `translateX(${-this.$textSpan.get(0).clientWidth * now}px)`);
                $textSpanClone.css('transform', `translateX(${-this.$textSpan.get(0).clientWidth * now}px)`);
            },
            complete: () => {
                this.$textSpan.css('transform', '');
                this.$textSpan.css('paddingRight', '');
                this.$textSpan.next().remove();

                this.timeout = null;
                this.startMarqueeAfterTimeout(this.interval);
            }
        });
    }

    startMarqueeAfterTimeout(timeout) {
        if (this.timeout !== null) return;

        if (this.$textSpan.get(0).clientWidth > this.$element.get(0).clientWidth) {
            this.timeout = setTimeout(() => {
                this.startMarquee();
            }, timeout * 1000);
        }
    }
}