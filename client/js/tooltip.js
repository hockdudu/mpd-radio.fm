import $ from 'jquery';
import tippy from 'tippy.js';

export default function configureTooltip() {
    tippy.setDefaults({
        touchHold: true,
        theme: 'google'
    });

    const callback = (mutationList) => {
        for (const mutation of mutationList) {
            const $target = $(mutation.target);
            const $tooltipItems = $target.find('[data-tippy-content]');
            if ($tooltipItems.length) {
                $tooltipItems.css('userSelect', 'none');
                tippy($tooltipItems.toArray());
            }
        }
    };

    const observer = new MutationObserver(callback);

    observer.observe($('body').get(0), {
        childList: true,
        subtree: true
    });
}