import $ from 'jquery';

export default class Modal {
    constructor(modalString) {
        this.$element = $(modalString);
        this.onAccept = null;
        this.onCancel = null;

        const $body = $('body');

        $body.append(this.$element);

        this.bindEvents();
    }

    bindEvents() {
        const $close = this.$element.find('.modal-close');
        const $cancel = this.$element.find('.modal-cancel');
        const $accept = this.$element.find('.modal-accept');
        const $wrapper = this.$element.filter('.modal-wrapper');

        $close.on('click', (event) => {
            if (typeof this.onCancel === 'function') this.onCancel(event, this.$element);
            this.$element.remove();
        });

        $cancel.on('click', (event) => {
            if (typeof this.onCancel === 'function') this.onCancel(event, this.$element);
            this.$element.remove();
        });

        $accept.on('click', (event) => {
            if (typeof this.onAccept === 'function') this.onAccept(event, this.$element);
            this.$element.remove();
        });

        $wrapper.on('click', (event) => {
            if (event.originalEvent.target !== event.currentTarget) return;
            if (typeof this.onCancel === 'function') this.onCancel(event, this.$element);
            this.$element.remove();
        });
    }
}