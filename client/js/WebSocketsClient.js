import EventEmitter from 'eventemitter3';

export default class WebSocketsClient extends EventEmitter {
    constructor(location) {
        super();
        this.url = location;
    }

    handleConnection() {
        console.log(`Trying to connect to ${this.url}`);
        this.client = new WebSocket(this.url);
        this.bindListeners();
    }

    retryConnect(time) {
        if (this.retryTimeout) return;

        console.log(`Trying to reconnect in ${time / 1000} seconds`);
        this.retryTimeout = setTimeout(() => {
            this.handleConnection(this.url);
            this.retryTimeout = null;
        }, time);
    }

    bindListeners() {
        this.client.onopen = (event) => {
            console.log(`Connected to ${this.url}`);
            this.emit('connect', event);
        };

        this.client.onmessage = (event) => {
            try {
                const parsedJson = JSON.parse(event.data);
                if (!parsedJson.error) {
                    this.messageListener(parsedJson);
                } else {
                    WebSocketsClient.errorHandler(parsedJson);
                }
            } catch (e) {
                console.error('Error while parsing JSON');
                console.error(e);
            }
        };

        this.client.onerror = (event) => {
            this.emit('error', event);
        };

        this.client.onclose = (event) => {
            this.emit('close', event);
        };
    }

    requestServerData() {
        const statusRequest = {type: 'status'};
        const stationsListRequest = {type: 'stations_list'};
        const currentStationRequest = {type: 'current_station'};
        const currentSongRequest = {type: 'current_song'};

        this.client.send(JSON.stringify(statusRequest));
        this.client.send(JSON.stringify(stationsListRequest));
        this.client.send(JSON.stringify(currentStationRequest));
        this.client.send(JSON.stringify(currentSongRequest));
    }

    static errorHandler(message) {
        console.error(message);
    }

    messageListener(message) {
        switch (message.type) {
            case 'online':
                this.emit('online', message);
                break;
            case 'current_station':
                this.emit('current-station', message);
                break;
            case 'stations_list':
                this.emit('stations-list', message);
                break;
            case 'status':
                this.emit('status', message);
                break;
            case 'current_song':
                this.emit('current-song', message);
                break;
        }
    }

    sendRadioChange(radioName) {
        const request = {
            type: 'change_station',
            data: {
                station_name: radioName
            }
        };

        this.client.send(JSON.stringify(request));
    }

    sendPause() {
        const request = {type: 'pause'};
        this.client.send(JSON.stringify(request));
    }

    sendPlay() {
        const request = {type: 'play'};
        this.client.send(JSON.stringify(request));
    }

    sendCreateStation(stationName, stationUrl) {
        const request = {type: 'new_station', data: {station_name: stationName, station_url: stationUrl}};
        this.client.send(JSON.stringify(request));
    }

    sendDeleteStation(stationName) {
        const request = {type: 'delete_station', data: {station_name: stationName}};
        this.client.send(JSON.stringify(request));
    }

    sendUpdateStation(stationName, newStationName, newStationUrl) {
        const request = {type: 'update_station', data: {station_name: stationName, new_station_name: newStationName, station_url: newStationUrl}};
        this.client.send(JSON.stringify(request));
    }

    sendVolume(newVolume) {
        const request = {type: 'volume', data: {volume: parseInt(newVolume)}};
        this.client.send(JSON.stringify(request));
    }
}