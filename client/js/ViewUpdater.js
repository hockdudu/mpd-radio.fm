import radioListTemplate from 'radio/radioList.njk';
import indexTemplate from 'index.njk';
import newRadioTemplate from 'modal/new_radio.njk';
import radioDeleteTemplate from 'modal/delete_radio.njk';
import radioEditTemplate from 'modal/edit_radio.njk';
import settingsTemplate from 'modal/settings.njk';

import $ from 'jquery';
import Modal from './Modal';
import noUiSlider from 'nouislider';
import Drop from 'tether-drop';
import EventEmitter from 'eventemitter3';
import Menu from './menu/Menu';
import MenuItem from './menu/MenuItem';
import Marquee from './Marquee';

export default class ViewUpdater extends EventEmitter {

    constructor() {
        super();
    }

    static updateSplashState(state) {
        const $body = $('body');
        if ($body.data('loaded')) return;

        const $loadingSplashState = $('.loading-splash-current-state');

        switch (state) {
            case 'connected':
                $loadingSplashState.html($loadingSplashState.data('textConnected'));
                break;
            case 'online': {
                $loadingSplashState.html($loadingSplashState.data('textOnline'));
                const $loadingSplash = $('.loading-splash');
                $loadingSplash.fadeOut({
                    complete: () => {
                        $loadingSplash.remove();
                    }
                });
                break;
            }
        }
    }

    prepareBody() {
        const $body = $('body');
        if ($body.data('loaded')) return;

        $body.data('loaded', true);
        $body.prepend(indexTemplate.render());
        ViewUpdater.updateDarkThemeStatus();


        const menu = new Menu($('.navbar-icons-wrapper'), 0, [
            new MenuItem('Add radio', 'add', () => {
                const newRadioModal = newRadioTemplate.render();
                const modal = new Modal(newRadioModal);
                modal.onAccept = (event, $modal) => {
                    const radioName = $modal.find('#radio-name').val();
                    const radioUrl = $modal.find('#radio-url').val();
                    this.emit('radio-add', radioName, radioUrl);
                };
            }),
            new MenuItem('Settings', 'settings', () => {
                const modal = new Modal(settingsTemplate.render({darkThemeEnabled: localStorage.darkTheme === 'true'}));
                const $darkThemeButton = modal.$element.find('#dark-theme');
                $darkThemeButton.on('change', (event) => {
                    localStorage.darkTheme = event.target.checked;
                    ViewUpdater.updateDarkThemeStatus();
                });
            }),
        ]);

        menu.render();
    }

    bindListeners() {
        // Play / Pause button
        const $fpPlayPause = $('.fp-play-pause');
        $fpPlayPause.on('click', (event) => {
            const isPlaying = event.target.dataset.status === 'playing';

            if (isPlaying) {
                this.emit('pause', event);
                ViewUpdater.updatePlayingStatus('stop');
            } else {
                this.emit('play', event);
                ViewUpdater.updatePlayingStatus('play');
            }
        });

        // Volume slider's slider
        const $volumeSlider = $('.fp-volume-slider');
        noUiSlider.create($volumeSlider.get(0), {
            start: [50],
            connect: [true, false],
            range: {
                'min': 0,
                'max': 100
            },
            step: 1,
            direction: 'rtl',
            orientation: 'vertical'
        }).on('slide', () => {
            const newVolume = $volumeSlider.get(0).noUiSlider.get();
            this.emit('volume', newVolume);
        });

        // Volume slider's button
        const $volumePopupToggle = $('.fp-volume-toggle');
        new Drop({
            target: $volumePopupToggle.get(0),
            content: $volumeSlider.get(0),
            position: 'top center',
            classes: 'drop-theme-arrows-bounce'
        }).on('open', () => {
            const $fpVolumeWrapper = $('.fp-volume-slider-wrapper');
            const volume = $fpVolumeWrapper.data('volume');
            $volumeSlider.get(0).noUiSlider.set(volume);
        });
    }

    updateRadioList(list) {
        const $main = $('main');
        $main.empty();

        const renderedRadioList = radioListTemplate.render({radioList: list});
        $main.html(renderedRadioList);

        const $playPause = $('.radio-play-button');
        $playPause.on('click', (event) => {
            const $target = $(event.target);
            const $parent = $target.closest('.radio-list-item');
            const radioName = $parent.data('radioName');
            this.emit('radio-change', radioName);
        });

        const $delete = $('.radio-delete');
        $delete.on('click', (event) => {
            const $target = $(event.target);
            const $parent = $target.closest('.radio-more-items');
            const radioName = $parent.data('radioName');

            const modal = new Modal(radioDeleteTemplate.render({radio_name: radioName}));
            modal.onAccept = () => {
                this.emit('radio-delete', radioName);
            };
        });

        const $edit = $('.radio-edit');
        $edit.on('click', (event) => {
            const $target = $(event.target);
            const $parent = $target.closest('.radio-more-items');
            const radioName = $parent.data('radioName');
            const radioUrl = $parent.data('radioUrl');

            const modal = new Modal(radioEditTemplate.render({radio_name: radioName, radio_url: radioUrl}));
            modal.onAccept = (event, $modal) => {
                const newRadioName = $modal.find('#radio-name').val();
                const radioUrl = $modal.find('#radio-url').val();

                this.emit('radio-edit', radioName, newRadioName, radioUrl);
            };
        });

        const $more = $('.radio-more');
        $more.each((index, element) => {
            const $element = $(element);
            const $menu = $element.siblings('.radio-more-items');

            const drop = new Drop({
                target: $element.get(0),
                content: $menu.get(0),
                position: 'bottom right',
                tetherOptions: {
                    targetAttachment: 'top right'
                }
            });

            $menu.on('click', () => {
                drop.close();
            });
        });
    }

    static updateCurrentStation(stationName) {
        const $fpRadioName = $('.fp-radio-name');
        $fpRadioName.html(stationName);

        if (!$fpRadioName.data('marquee')) {
            const marquee = new Marquee($fpRadioName);
            $fpRadioName.data('marquee', marquee);
        }

        $fpRadioName.data('marquee').marquee();
    }

    static updateCurrentSong(songName) {
        const $fpSongName = $('.fp-song-name');
        $fpSongName.html(songName);

        if (!$fpSongName.data('marquee')) {
            const marquee = new Marquee($fpSongName);
            $fpSongName.data('marquee', marquee);
        }

        $fpSongName.data('marquee').marquee();

    }

    static updatePlayingStatus(status) {
        const $fpPlayPause = $('.fp-play-pause');
        $fpPlayPause.removeClass('playing paused');

        $fpPlayPause.attr('data-status', status === 'play' ? 'playing' : 'paused');

        if ($fpPlayPause.get(0)._tippy) {
            $fpPlayPause.get(0)._tippy.setContent(status === 'play' ? $fpPlayPause.data('textPause') : $fpPlayPause.data('textPlay'));
        }
    }

    static updateOnlineStatus(isOnline) {
        const $onlineNotifierWrapper = $('.fp-online-status-wrapper');
        const $onlineNotifier = $('.fp-online-status');
        if (isOnline) {
            if ($onlineNotifier.attr('data-status') !== 'online') {
                $onlineNotifierWrapper.slideUp({
                    complete: () => {
                        $onlineNotifierWrapper.slideDown();
                        $onlineNotifier.html($onlineNotifier.data('textOnline'));
                        $onlineNotifier.attr('data-status', 'online');

                        setTimeout(() => {
                            $onlineNotifierWrapper.slideUp();
                        }, 2000);
                    }
                });

            }
        } else {
            $onlineNotifierWrapper.slideDown();
            $onlineNotifier.html($onlineNotifier.data('textOffline'));
            $onlineNotifier.attr('data-status', 'offline');
        }
    }

    static updateVolume(newVolume) {
        const $fpVolumeWrapper = $('.fp-volume-slider-wrapper');
        $fpVolumeWrapper.data('volume', newVolume);
    }

    static updateDarkThemeStatus() {
        const $body = $('body');
        const isDarkThemeEnabled = localStorage.darkTheme === 'true';

        if (isDarkThemeEnabled) {
            $body.addClass('dark');
        } else {
            $body.removeClass('dark');
        }
    }
}