import $ from 'jquery';
import menuTemplate from 'menu/menu.njk';
import Drop from 'tether-drop';

export default class Menu {

    /**
     * Creates a Menu object with the given parameters
     * @param $wrapper The element the menu should be inserted into
     * @param minShown The minimum number of menu items to be shown. If there are more items than minShown, a dropdown will be shown on small screens. No dropdown will be shown if this number is less than 0
     * @param items A array with MenuItem to be rendered
     */
    constructor($wrapper, minShown = 0, items = []) {
        this.$wrapper = $wrapper;
        this.minShown = minShown;
        this.items = items;
    }

    /**
     * Renders the menu and adds it to the specified wrapper. Listeners are also bound.
     */
    render() {
        const renderedMenu = menuTemplate.render({minShown: this.minShown, items: this.items, dropdownAvailable: this.isDropdownAvailable()});
        this.$wrapper.html(renderedMenu);

        this.$wrapper.find('[data-menu-index]').each((index, element) => {
            const $clickedElement = $(element);
            const elementIndex = $(element).data('menuIndex');
            const menuItem = this.items[elementIndex];
            $clickedElement.on('click', menuItem.callback);
        });

        this.$toggle = this.$wrapper.find('.navbar-dropdown-toggle');
        this.$dropdown = this.$wrapper.find('.navbar-dropdown-items');

        $(window).off('.menu');
        if (this.isDropdownAvailable()) {
            const drop = new Drop({
                target: this.$toggle.get(0),
                content: this.$dropdown.get(0),
                position: 'bottom right',
                tetherOptions: {
                    targetAttachment: 'top right'
                }
            });

            this.$dropdown.on('click', () => {
                drop.close();
            });

            $(window).on('resize.menu', () => this.handleResize());
            this.handleResize();
        }
    }

    /**
     * Returns whether the dropdown should be rendered, depending on the number of items on the menu
     * @returns {boolean}
     */
    isDropdownAvailable() {
        return this.items.length > this.minShown + 1 && this.minShown >= 0;
    }

    /**
     * Shows or hides menu items depending on the screen size
     */
    handleResize() {
        if (!this.isDropdownAvailable()) return;
        const scrollWidth = () => this.$wrapper.get(0).scrollWidth;
        const realWidth = () => this.$wrapper.get(0).clientWidth;

        if (scrollWidth() !== realWidth()) {
            const $visibleNonPersistent = () => this.$wrapper.find('[data-menu-persistent="false"][data-menu-shown="true"]');
            this.$toggle.attr('data-menu-shown', true);

            do {
                $visibleNonPersistent().last().attr('data-menu-shown', false);
            } while(scrollWidth() !== realWidth() && $visibleNonPersistent().length !== 0);
        } else {
            const $invisibleNonPersistent = () => this.$wrapper.find('[data-menu-persistent="false"][data-menu-shown="false"]');

            do {
                // It makes no sense to have a "more" button with only one icon, so we fix it here
                if ($invisibleNonPersistent().length <= 2) {
                    $invisibleNonPersistent().attr('data-menu-shown', true);
                    this.$toggle.attr('data-menu-shown', false);
                } else {
                    $invisibleNonPersistent().first().attr('data-menu-shown', true);
                }
            } while (scrollWidth() === realWidth() && $invisibleNonPersistent().length !== 0);

            if (scrollWidth() !== realWidth()) {
                this.handleResize();
            }
        }

        const visibleIndexes = this.$wrapper.find('[data-menu-index][data-menu-shown="true"]').toArray().map((element) => $(element).attr('data-menu-index'));
        this.$dropdown.find('[data-menu-index]').each((index, element) => {
            const $element = $(element);

            // Dropdown item is only visible if menu item isn't
            $element.attr('data-menu-shown', visibleIndexes.indexOf($element.attr('data-menu-index')) === -1);
        });
    }
}