export default class MenuItem {

    /**
     * Holds information for the menu items used on the Menu
     * @param name Name to be shown on labels and on the dropdown
     * @param icon Icon to be used
     * @param callback Function to be called when the item is clicked on
     */
    constructor(name, icon, callback) {
        this.name = name;
        this.icon = icon;
        this.callback = callback;
    }
}