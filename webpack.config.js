const path = require('path');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');

module.exports = (env, argv) => {
    const devMode = argv.mode !== 'production';

    const plugins = [
        new MiniCssExtractPlugin({
            filename: devMode ? "[name].css" : "[name].[hash].css",
            chunkFilename: devMode ? "[id].css" : "[id].[hash].css"
        }),
        new HtmlWebpackPlugin({
            title: 'My App',
            filename: 'index.html',
            template: 'client/index.html'
        }),
    ];

    if (!devMode) plugins.push(new WebpackCleanupPlugin());

    return {
        entry: ['./client/js/main.js', './client/scss/main.scss'],
        output: {
            filename: devMode ? "[name].js" : "[name].[hash].js",
            path: path.resolve(__dirname, 'static')
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                },
                {
                    test: /\.scss$/,
                    use: [{
                        loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader
                    }, {
                        loader: "css-loader", options: {
                            sourceMap: true
                        }
                    }, {
                        loader: "sass-loader", options: {
                            sourceMap: true
                        }
                    }]
                },
                {
                    test: /\.(njk)$/,
                    loader: 'nunjucks-loader'
                },
                {
                    test: /\.(ttf|woff2|woff|eot)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                emitFile: true,
                                outputPath: 'fonts/'
                            }
                        }
                    ]
                }
            ]
        },
        plugins: plugins,
        resolve: {
            modules: [path.resolve(__dirname, 'client/templates'), 'node_modules']
        }

    };
};
