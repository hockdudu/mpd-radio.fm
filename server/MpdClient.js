import EventEmitter from 'events';
import {MPC} from 'mpc-js';

export default class MpdClient extends EventEmitter {
    constructor(env) {
        super();
        this.env = env;

        this.isConnected = false;

        this.handleConnection();
    }

    handleConnection() {
        if (this.client) delete this.client;

        this.client = new MPC();

        this.connect();
        this.bindListeners();
    }

    connect() {
        console.log(`Trying to connect to MPD on tcp://${this.env.mpd_host}:${this.env.mpd_port}...`);

        // It's already caught on 'socket-error', so an empty catch block is ok
        this.client.connectTCP(this.env.mpd_host, this.env.mpd_port).catch(() => {
        });
    }

    bindListeners() {
        this.client.on('ready', () => {
            this.isConnected = true;
            this.broadcastOnlineChange();

            console.log(`Successfully connected to MPD on tcp://${this.env.mpd_host}:${this.env.mpd_port}...`);
        });

        this.client.on('changed-player', () => {
            this.broadcastStatusChange();
        });

        this.client.on('changed-mixer', () => {
            this.broadcastStatusChange();
        });

        this.client.on('changed-playlist', () => {
            this.broadcastStatusChange();
            this.broadcastStationChange();
            this.broadcastSongChange();
        });

        this.client.on('changed-stored_playlist', () => {
            this.broadcastStationsListChange();
        });

        this.client.on('socket-end', () => {
            this.isConnected = false;
            this.broadcastOnlineChange();

            console.warn(`Disconnected from tcp://${this.env.mpd_host}:${this.env.mpd_port}, reconnecting in ${this.env.reconnect_time / 1000} seconds`);

            setTimeout(() => {
                this.handleConnection();
            }, this.env.reconnect_time);
        });

        this.client.on('socket-error', (err) => {
            this.isConnected = false;
            this.broadcastOnlineChange();

            console.error(`Connection failed, retrying in ${this.env.reconnect_time / 1000} seconds`);
            console.error(err);

            setTimeout(() => {
                this.handleConnection();
            }, this.env.reconnect_time);
        });
    }

    broadcastStatusChange() {
        this.getStatus((err, msg) => {
            this.emit('status-change', err, msg);
        });
    }

    broadcastOnlineChange() {
        this.emit('online-change', null, {connected: this.isConnected});
    }

    broadcastStationChange() {
        this.getCurrentRadio((err, msg) => {
            this.emit('station-change', err, msg);
        }, true);
    }

    broadcastSongChange() {
        this.getSongName((err, msg) => {
            this.emit('song-change', err, msg);
        });
    }

    broadcastStationsListChange() {
        this.getRadioList((err, msg) => {
            this.emit('station-list-change', err, msg);
        });
    }

    static generateServerError(code, message) {
        return {
            errorCode: code,
            isServerError: true,
            errorMessage: message
        };
    }

    // Client calls

    play(callback) {
        this.client.playback.play().catch((err) => callback(err));
    }

    pause(callback) {
        this.client.playback.stop().catch((err) => callback(err));
    }

    getCurrentRadio(callback, ignoreErrors) {
        const currentSong = this.client.status.currentSong();

        const playlistInfoList = new Promise((resolve, reject) => {
            const playlistsList = this.client.storedPlaylists.listPlaylists();

            playlistsList.then((playlistsList) => {
                const promisesArray = playlistsList.map(playlist => {
                    return new Promise((resolve, reject) => {
                        this.client.storedPlaylists.listPlaylist(playlist.name).then((songsArray) => {
                            resolve({
                                name: playlist.name,
                                songs: songsArray
                            });
                        }).catch((err) => {
                            reject(err);
                        });
                    });
                });

                Promise.all(promisesArray).then((values) => {
                    resolve(values);
                }).catch((err) => {
                    reject(err);
                });
            }).catch((err) => {
                reject(err);
            });
        });

        Promise.all([currentSong, playlistInfoList]).then(([currentSong, playlistInfoList]) => {
            if (typeof currentSong === 'undefined') {
                currentSong = {path: ''};
            }

            const currentPlaylist = playlistInfoList.find(playlist => playlist.songs.includes(currentSong.path));

            if (typeof currentPlaylist !== 'undefined') {
                callback(null, {station_name: currentPlaylist.name});
            } else {
                if (!ignoreErrors) callback(MpdClient.generateServerError(1, 'Current radio couldn\'t be found'));
            }
        }).catch((err) => {
            if (!ignoreErrors) callback(err);
        });
    }

    changeRadio(radioName, callback) {
        this.client.currentPlaylist.clear()
            .then(() => this.client.storedPlaylists.load(radioName))
            .then(() => this.client.playback.play())
            .catch((err) => callback(err));
    }

    getRadioList(callback) {
        this.client.storedPlaylists.listPlaylists().then((playlists) => {
            const playlistsSongsPromise = playlists.map((playlist) => new Promise((resolve, reject) => {
                this.client.storedPlaylists.listPlaylist(playlist.name).then((songsArray) => {
                    resolve({
                        name: playlist.name,
                        url: songsArray.length ? songsArray[0] : ''
                    });
                }).catch((err) => {
                    reject(err);
                });
            }));

            Promise.all(playlistsSongsPromise).then((values) => {
                values.sort((a, b) => a.name.localeCompare(b.name));
                callback(null, {stations: values});
            }).catch((err) => {
                callback(err);
            });
        }).catch((err) => {
            callback(err);
        });
    }

    getStatus(callback) {
        this.client.status.status().then((status) => {
            callback(null, status);
        }).catch((err) => {
            callback(err);
        });
    }

    getSongName(callback) {
        this.client.status.currentSong().then((currentSong) => {
            if (typeof currentSong === 'undefined') {
                currentSong = {title: '', name: '', path: ''};
            }

            callback(null, {
                title: currentSong.title,
                name: currentSong.name,
                path: currentSong.path
            });
        }).catch((err) => {
            callback(err);
        });
    }

    setVolume(newVolume, callback) {
        this.client.playbackOptions.setVolume(newVolume).catch((err) => {
            callback(err);
        });
    }

    createStation(stationName, stationUrl, callback) {
        this.client.storedPlaylists.listPlaylist(stationName).then(() => {
            callback(MpdClient.generateServerError(2, 'Station already exists'));
        }).catch(() => {
            this.client.storedPlaylists.playlistAdd(stationName, stationUrl).catch((err) => {
                callback(err);
            });
        });
    }

    deleteStation(stationName, callback) {
        this.client.storedPlaylists.remove(stationName).catch((err) => {
            callback(err);
        });
    }

    changeStationNameAndUrl(oldStationName, newStationName, stationUrl, callback) {
        if (oldStationName !== newStationName) {
            this.client.storedPlaylists.listPlaylist(newStationName).then(() => {
                callback(MpdClient.generateServerError(3, 'An playlist with this name already exists'));
            }).catch(() => {
                this.client.storedPlaylists.playlistAdd(newStationName, stationUrl).then(() => {
                    this.client.storedPlaylists.remove(oldStationName).catch((err) => {
                        callback(err);
                    });
                }).catch((err) => {
                    callback(err);
                });
            });
        } else {
            this.client.storedPlaylists.playlistClear(oldStationName).then(() => {
                this.client.storedPlaylists.playlistAdd(oldStationName, stationUrl).catch((err) => {
                    callback(err);
                });
            }).catch((err) => {
                callback(err);
            });
        }
    }

    getOnlineStatus(callback) {
        callback(null, {connected: this.isConnected});
    }
}