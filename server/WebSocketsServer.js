import WebSocket from 'ws';

export default class WebSocketsServer {
    constructor(server, mpdClient) {
        this.wss = new WebSocket.Server({server});

        this.wss.on('connection', (ws) => {
            this.connectionListener(ws);
        });

        this.mpdClient = mpdClient;

        this.mpdClient.on('status-change', (err, msg) => this.statusBroadcastListener(err, msg));
        this.mpdClient.on('online-change', (err, msg) => this.onlineBroadcastListener(err, msg));
        this.mpdClient.on('station-change', (err, msg) => this.stationBroadcastListener(err, msg));
        this.mpdClient.on('song-change', (err, msg) => this.songBroadcastListener(err, msg));
        this.mpdClient.on('station-list-change', (err, msg) => this.stationListBroadcastListener(err, msg));
    }

    connectionListener(ws) {
        ws.on('message', (message) => {
            try {
                const jsonMessage = JSON.parse(message);
                this.messageListener(ws, jsonMessage);
            } catch (e) {
                console.error(e);
            }
        });

        // Makes the serve send it's online status as soon the client connects
        this.messageListener(ws, {type: 'online'});
    }

    messageListener(ws, msg) {
        switch (msg.type) {
            case 'current_station':
                this.mpdClient.getCurrentRadio((err, msg) => {
                    WebSocketsServer.sendMessage('current_station', err, msg, ws);
                });
                break;
            case 'stations_list':
                this.mpdClient.getRadioList((err, msg) => {
                    WebSocketsServer.sendMessage('stations_list', err, msg, ws);
                });
                break;
            case 'change_station':
                if (msg.data && msg.data.station_name) {
                    this.mpdClient.changeRadio(msg.data.station_name, (err, msg) => {
                        WebSocketsServer.sendMessage('change_station', err, msg, ws);
                    });
                }
                break;
            case 'play':
                this.mpdClient.play((err, msg) => {
                    WebSocketsServer.sendMessage('play', err, msg, ws);
                });
                break;
            case 'pause':
                this.mpdClient.pause((err, msg) => {
                    WebSocketsServer.sendMessage('pause', err, msg, ws);
                });
                break;
            case 'volume':
                if (msg.data && typeof msg.data.volume === 'number') {
                    this.mpdClient.setVolume(msg.data.volume, (err, msg) => {
                        WebSocketsServer.sendMessage('volume', err, msg, ws);
                    });
                }
                break;
            case 'status':
                this.mpdClient.getStatus((err, msg) => {
                    WebSocketsServer.sendMessage('status', err, msg, ws);
                });
                break;
            case 'current_song':
                this.mpdClient.getSongName((err, msg) => {
                    WebSocketsServer.sendMessage('current_song', err, msg, ws);
                });
                break;
            case 'new_station':
                if (msg.data && msg.data.station_name && msg.data.station_url) {
                    this.mpdClient.createStation(msg.data.station_name, msg.data.station_url, (err, msg) => {
                        WebSocketsServer.sendMessage('new_station', err, msg, ws);
                    });
                }
                break;
            case 'delete_station':
                if (msg.data && msg.data.station_name) {
                    this.mpdClient.deleteStation(msg.data.station_name, (err, msg) => {
                        WebSocketsServer.sendMessage('delete_station', err, msg, ws);
                    });
                }
                break;
            case 'update_station':
                if (msg.data && msg.data.station_name && msg.data.new_station_name && msg.data.station_url) {
                    this.mpdClient.changeStationNameAndUrl(msg.data.station_name, msg.data.new_station_name, msg.data.station_url, (err, msg) => {
                        WebSocketsServer.sendMessage('update_station', err, msg, ws);
                    });
                }
                break;
            case 'online':
                this.mpdClient.getOnlineStatus((err, msg) => {
                    WebSocketsServer.sendMessage('online', err, msg, ws);
                });
                break;
        }
    }

    statusBroadcastListener(err, msg) {
        this.wss.clients.forEach(client => {
            WebSocketsServer.sendMessage('status', err, msg, client);
        });
    }

    onlineBroadcastListener(err, msg) {
        this.wss.clients.forEach(client => {
            WebSocketsServer.sendMessage('online', err, msg, client);
        });
    }

    stationBroadcastListener(err, msg) {
        this.wss.clients.forEach(client => {
            WebSocketsServer.sendMessage('current_station', err, msg, client);
        });
    }

    songBroadcastListener(err, msg) {
        this.wss.clients.forEach(client => {
            WebSocketsServer.sendMessage('current_song', err, msg, client);

        });
    }

    stationListBroadcastListener(err, msg) {
        this.wss.clients.forEach(client => {
            WebSocketsServer.sendMessage('stations_list', err, msg, client);
        });
    }

    static sendMessage(type, err, data, ws) {
        const message = {
            type: type,
            error: !!err,
            data: err ? err : data
        };

        ws.send(JSON.stringify(message));
    }
}