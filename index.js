import {config as envConfig} from 'dotenv';
import express from 'express';
import http from 'http';
import MpdClient from './server/MpdClient';
import WebSocketsServer from './server/WebSocketsServer';

envConfig();

const env = {
    mpd_host: process.env.MPD_HOST,
    mpd_port: process.env.MPD_PORT,
    server_port: process.env.SERVER_PORT,
    reconnect_time: process.env.RECONNECT_TIME
};

const app = express();
const server = http.createServer(app);

const mpdClient = new MpdClient(env);
new WebSocketsServer(server, mpdClient);

app.use(express.static(`${__dirname}/static`));

server.listen(env.server_port, () => console.log(`Listening on port ${env.server_port}!`));


