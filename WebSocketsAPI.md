# Format specification

The data sent and received from the server are stringified JSON.

The actual data from the JSON varies with the request, but all of them have the following schema:

```json
{
    "type": "message_type",
    "error": false,
    "data": {
        "something": "value",
        "something_else": "other_value"
    }
}
```

Messages sent to the server don't have the `error` property (it wouldn't make sense)

# Requests

The available request types are the following:

## current_station
### Description
Asks the server to send the current station's name
### Body
```json
{
    "type": "current_station"
}
```

## stations_list
### Description
Asks the server to send a list with the available stations
### Body
```json
{
    "type": "stations_list"
}
```

## change_station
### Description
Changes the current playing radio station
### Body
```json
{
    "type": "change_station",
    "data": {
        "station_name": "Radio XYZ"
    }
}
```
## play
### Description
Plays the radio, if it's paused
### Body
```json
{
    "type": "play"
}
```

## pause
### Description
Pauses the radio, if it's playing
### Body
```json
{
    "type": "pause"
}
```

## volume
### Description
Changes the radio's volume
### Body
```json
{
    "type": "volume",
    "data": {
        "volume": 50
    }
}
```

## status
### Description
Asks the server to send the player's actual status
### Body
```json
{
    "type": "status"
}
```

## current_song
### Description
Asks the server for the current song's name
### Body
```json
{
    "type": "current_song"
}
```

## new_station
### Description
Creates a new radio station with the given name and url
### Body
```json
{
    "type": "new_station",
    "data": {
        "station_name": "Radio XYZ",
        "station_url": "http://example.com/stream/mp3_128"
    }
}
```

## delete_station
### Description
Deletes the given radio from the server
### Body
```json
{
    "type": "delete_station",
    "data": {
        "station_name": "Radio XYZ"
    }
}
```

## update_station
### Description
Updates the playlist's name and url
### Body
```json
{
    "type": "update_station",
    "data": {
        "station_name": "Radio XYZ",
        "new_station_name": "Radio ABC",
        "station_url": "http://example.com/stream/mp3_128"
    }
}
```

## online
### Descriptions
Asks the server if it is connected with the MPD server
### Body
```json
{
    "type": "online"
}
```

# Responses
The server responds to it's requests accordingly.
It may also send the responses without the client asking for them, e.g. the server always broadcasts the new player status when it changes.

For every request there is a response with the same name. Some only are sent when an error occurs (e.g. `volume`). Those aren't listed here.

## current_station
### Description
The client has requested the current station or it changed
### Sample response
```json
{
    "type": "current_station",
    "error": false,
    "data": {
        "station_name": "Radio XYZ"
    }
}
```

## stations_list
### Description
The client requested the stations list
### Sample response
```json
{
    "type": "stations_list",
    "error": false,
    "data": {
        "stations": [
            {
                "name": "Radio ABC",
                "url": "http://example.com/stream/mp3_128"
            },
            {
                "name": "Radio XYZ",
                "url": "http://example.com/stream/mp3_128"
            }
        ]
    }
}
```

## status
### Description
The client requested the status or it changed.

**Attention**: When the current station changes, status is fired multiple times (because the server clears the current playlist, adds the new radio and plays it; all those actions fire events)

### Sample response
```json
{
    "type": "status",
    "error": false,
    "data": {
        "state": "play",
        "song": 2,
        "songId": 5,
        "nextSong": 2,
        "nextSongId": 5,
        "elapsed": 2303.364,
        "duration": null,
        "bitRate": 128,
        "audio": "44100:24:2",
        "sampleRate": 44100,
        "bitDepth": 24,
        "channels": 2,
        "volume": 100,
        "xfade": null,
        "mixrampdb": 0,
        "mixrampDelay": null,
        "playlistVersion": 77,
        "playlistLength": 3,
        "repeat": true,
        "random": false,
        "single": true,
        "consume": false,
        "updating": null
    }
}
```

## current_song
### Description
The client requested the current song's name or it changed
### Sample response
```json
{
    "type": "current_song",
    "error": false,
    "data": {
        "title": "Song Name - Artist",
        "name": "Usually The Radio's Name",
        "path": "http://example.com/stream/mp3_128"
    }
}
```

## online
### Description
It's fired when the server's online status changes (e.g. MPD goes offline or online)
### Sample response
```json
{
    "type": "online",
    "error": false,
    "data": {
        "connected": true
    }
}
```

# Error handling
Always when an error occur, the client is informed. There are two types of errors: mpd errors and server errors.

mpd errors have following format:
```json
{
    "type": "stations_list",
    "error": true,
    "data": {
        "errorCode": 50,
        "errorMessage": "No such playlist"
    }
}
```

Server errors, in turn, have following format:
```json
{
    "type": "current_station",
    "error": true,
    "data": {
        "errorCode": 1,
        "isServerError": true,
        "errorMessage": "Current radio couldn't be found"
    }
}
```

In other words, server errors have the `isServerError` property.

## Possible server errors
- **1**: Current radio couldn't be found (when querying for the current radio)
- **2**: Station already exists (when creating a station)